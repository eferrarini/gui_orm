//graph para el diagrama uml
var graphMain = new joint.dia.Graph();
//paper para manejar el graph diagrama class
var paper = new joint.dia.Paper({
    el: document.getElementById('paper'),
    width: $('#paper').width(),
    height: $('#paper').height(),
    model: graphMain,
    gridSize: 10,
    snapLinks: { radius: 75 },
    linkPinning: false,
    markAvailable: true,
    defaultLink: createLinkProperty2Property(),
    interactive: { vertexAdd: true },
    validateConnection: function (cellViewS, magnetS, cellViewT, magnetT, end, linkView) {
        //solo usado para conectar ports. los otros enlaces se verifican manualmente.
        //solo se pueden conectar ports con ports. No se puede conectar un port con si mismo
        if (magnetT && magnetT.getAttribute('port-group') === 'link2link' && cellViewS!=cellViewT) {
            linkView.addTools(addToolsToDefaultLink());
            return true;
        }
        return false;
    },
/*
		drawGrid: {
			name: 'doubleMesh',
			args: [{
					color: '#808080',
					thickness: 1
				},
				{
					color: '#808080',
					scaleFactor: 10,
					thickness: 5
				}
			]
		}
        */
    });

function addToolsToDefaultLink(){
    var verticesTool = new joint.linkTools.Vertices({
        focusOpacity: 0.5,
        snapRadius: 5,
    });
    var segmentsTool = new joint.linkTools.Segments();
    var sourceAnchorTool = new joint.linkTools.SourceAnchor();
    var targetAnchorTool = new joint.linkTools.TargetAnchor();
    var boundaryTool = new joint.linkTools.Boundary();
    var removeButton = new joint.linkTools.Remove({
        distance: 20,
    });

    var toolsView = new joint.dia.ToolsView({
        tools: [
        verticesTool, segmentsTool,
        sourceAnchorTool, targetAnchorTool,
        boundaryTool, removeButton
        ]
    });
    return toolsView;
}
///////////////////////////////////////////////////////////////////////
/////Metodos auxiliares para facilitar las comparaciones de nombres
//////////////////////////////////////////////////////////////////////
function getSpecificType(elementModel) {
    var type = elementModel.attributes.type;
    var resultType;
    if (type === 'uml.Class') {
        resultType = 'Class';
    } else if (type === 'uml.Association') {
        resultType = 'Association';
    } else if (type === 'erd.Relationship') {
        resultType = 'Nary';
    } else if (type === 'erd.Inheritance') {
        resultType = 'Inheritance';
    } else if (type === 'standard.Link'){
        resultType = 'Link';
    } else if (type === 'tydevs.Model'){
        resultType = 'Model';
    } else {
        resultType = 'Error';
    }
    return resultType;
}

function getType(elementModel) {
    var type = elementModel.attributes.type;
    var resultType;
    if (type === 'uml.Class') {
        resultType = 'Class';
    } else if (type === 'uml.Association') {
        resultType = 'Association';
    } else if (type === 'erd.Inheritance') {
        resultType = 'Inheritance';
    } else if (type === 'erd.Relationship') {
        resultType = 'Nary';
    } else if (type === 'standard.Link'){
        resultType = 'Link';
    } else if (type === 'devs.Model'){
        resultType = 'Model';
    } else {
        resultType = 'Error';
    }
    return resultType;
}
///////////////////////////////////
///////////////////////////////////
///////////////////////////////////

/*devuelve true si se puede conectar dos elementos. 
Se pueden  conectar clases con clases, clases con herencia,
clases con relaciones Nary.*/
function canConnect(cellViewS, cellViewT) {
	var canConnect = false;
	if (cellViewS != null && cellViewT != null) {
		var sourceType = getType(cellViewS);
		var targetType = getType(cellViewT);
		var specificSourceType = getSpecificType(cellViewS);
		var specificTargetType = getSpecificType(cellViewT);
		//var linksS = getElementLinks(cellViewS);
		//var linksT = getElementLinks(cellViewT);
		//console.log(linksS);
		//console.log(linksT);
		if (sourceType === 'Class' && targetType === 'Class') {
			canConnect = true;
		} else if ((sourceType === 'Inheritance' && targetType === 'Class')||(sourceType=== 'Class' && targetType === 'Inheritance')) {
            canConnect = true;
        }else if ((sourceType === 'Nary' && targetType === 'Class')||( sourceType=== 'Class' && targetType === 'Nary')){
            canConnect = true;
        }
    } 
    return canConnect;
}

//graph para la paleta
var paletteGraph = new joint.dia.Graph();
//graph para manejar la paleta
var palette = new joint.dia.Paper({
  el: document.getElementById('palette'),
  width: $('#palette').width(),
  height: $('#palette').height(),
  model: paletteGraph,
  interactive: false,
		//gridSize: 100,
		/*drawGrid:
	{
		name: 'mesh',
		args: [
	{
		color: 'white',
		thickness: 1
		}
		]
  }*/
});

// var paperSmall = new joint.dia.Paper({
//         el: document.getElementById('minimap'),
//         model: graphMain,
//         width: $('#minimap').width(),
//         height: $('#minimap').height(),
//         gridSize: 1,
//         interactive: false
//     });
// paperSmall.scale(0.25);

//////////////////////////////////////////////////////
////////////////////////////////FUNCIONES DE LA PALETA
//////////////////////////////////////////////////////

//para el drag and drop de la paleta
palette.on('cell:pointerdown', function (cellView, e, x, y) {
    $('body').append('<div id="flyPaper"></div>');
    var flyGraph = new joint.dia.Graph,
    flyPaper = new joint.dia.Paper({
        el: $('#flyPaper'),
        model: flyGraph,
        interactive: false,
    }),
    flyShape = cellView.model.clone(),
    pos = cellView.model.position(),
    offset = {
        x: flyShape.attributes.size.width / 2 * paper.scale().sx,
        y: flyShape.attributes.size.height / 2 * paper.scale().sy
    };
    flyPaper.scale(paper.scale().sx);
    flyShape.position(0, 0);
    flyGraph.addCell(flyShape);
    $("#flyPaper").offset({
        left: (e.pageX - offset.x),
        top: (e.pageY - offset.y)
    });
    $('body').on('mousemove.fly', function (e) {
        $("#flyPaper").offset({
            left: (e.pageX - offset.x),
            top: (e.pageY - offset.y)
        });
    });
    $('body').on('mouseup.fly', function (e) {
        var x = e.pageX,
        y = e.pageY,
        target = paper.$el.offset();
        origin = palette.$el.offset();
        // Dropped over paper and not over origin
        if ((x > target.left && x < target.left + paper.$el.width() && y > target.top && y < target.top + paper.$el.height()) &&
            !(x > origin.left && x < origin.left + palette.$el.width() && y > origin.top && y < origin.top + palette.$el.height())) {
            var newElement = flyShape.clone();
        var relativePoint = paper.clientToLocalPoint(e.clientX, e.clientY);
            /*var localRect1 = paper.clientToLocalRect(target.left,target.top,target.width,target.height);
            newElement.position(((x - target.left - offset.x)+localRect1.center().x), ((y - target.top - offset.y)+localRect1.center().y));*/
            newElement.position(relativePoint.x - (newElement.attributes.size.width / 2), relativePoint.y - (newElement.attributes.size.height / 2));
            if (getType(cellView.model)!='Nary'){
                graphMain.addCell(newElement);
            }else{
            //si se selecciona una relacion Nary, crear la relacion binaria con clase.
            dragNaryToGraph(newElement, relativePoint);
        }
    }
    $('body').off('mousemove.fly').off('mouseup.fly');
    flyShape.remove();
    $('#flyPaper').remove();
});
});

//Cuando se usa la relacion con clase (el rombo), se crean las clases y las relaciones con el rombo.
function dragNaryToGraph(rombo, point){
//buscar el elemento uml.Class de la paleta
var classAux = null;
for (var i = paletteElements.length - 1; i >= 0; i--) {
    if (getType(paletteElements[i])=='Class'){
        classAux = paletteElements[i];
        break;
    }
}
    //agregar los elementos al graph
    graphMain.addCell(rombo);
    var clsA = classAux.clone();
    clsA.position(point.x - 220, (point.y - (rombo.attributes.size.height / 2))-20);
    var clsB = classAux.clone();
    clsB.position(point.x + 120, (point.y - (rombo.attributes.size.height / 2))-20);
    var clsC = classAux.clone();
    clsC.position(point.x-50, (point.y - (rombo.attributes.size.height / 2))+90);
    graphMain.addCell(clsA);
    graphMain.addCell(clsB);
    graphMain.addCell(clsC);
    var newLinkA = createLinkClass('class2rombo');
    connectLink(newLinkA, clsA, rombo);
    setAttrsLinkClass2Rombo(newLinkA);
    var newLinkB = createLinkClass('class2rombo');
    connectLink(newLinkB, clsB, rombo);
    setAttrsLinkClass2Rombo(newLinkB);
    var newLinkC = createLinkClassDashed();
    connectLink(newLinkC, clsC, rombo);
    addPortToLink(newLinkA);
    addPortToLink(newLinkB);
}

//para mover la paleta
var dragStartPositionPalette;
palette.on('blank:pointerdown', function (event, x, y) {
	if (!fixedPalette) {
		dragStartPositionPalette = {
			x: x,
			y: y
		};

		$('body').on('mousemove.fly', function (event) {
			if (dragStartPositionPalette != null) {
				$("#palette").offset({
					left: event.pageX - dragStartPositionPalette.x, //$("#palette").width() / 2,
					top: event.pageY - dragStartPositionPalette.y //$("#palette").height() / 2
				});
			}
		});

		$('body').on('mouseup.fly', function (e) {
			dragStartPositionPalette = null;
			$('body').off('mousemove.fly').off('mouseup.fly');
		});
	}
});

// herramientas para la paleta
var fixedPalette = true;
var opacityPalette = false;
var extendedPalette = false;
var horizontalPalette = false;

var paletteTools = $('<div id="paletteTools" class="toolbar-palette">');
paletteTools.append('<div id="paletteFixButton" class="tools tools-palette-fix" onclick="paletteFix()"><a class="tooltips" href="#"><i class="material-icons">lock</i><span>Unlock palette translate</span></a></div>');
paletteTools.append('<div id="paletteResetButton" class="tools tools-palette-reset" onclick="paletteReset()"><a class="tooltips" href="#"><i class="material-icons">picture_in_picture_alt</i><span>Reset default position</span></a></div>');
paletteTools.append('<div id="paletteOpacityButton" class="tools tools-palette-opacity" onclick="paletteOpacity()"><a class="tooltips" href="#"><i class="material-icons">visibility</i><span>Change palette opacity</span></a></div>');
paletteTools.append('<div id="paletteExtendButton" class="tools tools-palette-extend" onclick="paletteExtend()"><a class="tooltips" href="#"><i class="material-icons">add_circle</i><span>Extend palette elements</span></a></div>');
//paletteTools.append('<div id="paletteRotateButton" class="tools tools-palette-rotate" onclick="paletteRotate()"><i class="material-icons">rotate_90_degrees_ccw</i></div>');
paletteTools.css('display', 'none');

$('#palette').append(paletteTools);

$("#palette").mouseover(function () {
	paletteTools.css('display', 'block');
});

$("#palette").mouseleave(function () {
	paletteTools.css('display', 'none');
});

function paletteFix() {
	fixedPalette = !fixedPalette;
	if (fixedPalette) {
		document.getElementById("paletteFixButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">lock</i><span>Unlock palette translate</span></a>';
	} else {
		document.getElementById("paletteFixButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">lock</i><span>Lock palette translate</span></a>';
	}
}

function paletteReset() {
	var lastOpacity = document.getElementById("palette").style.opacity;
	var lastHeight = document.getElementById("palette").style.height;
	var lastWidth = document.getElementById("palette").style.width;
	document.getElementById("palette").style = null;
	document.getElementById("palette").style.opacity = lastOpacity;
	document.getElementById("palette").style.height = lastHeight;
	document.getElementById("palette").style.width = lastWidth;
}

function paletteOpacity() {
	opacityPalette = !opacityPalette;
	if (opacityPalette) {
		document.getElementById("paletteOpacityButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">visibility_off</i><span>Change palette opacity</span></a>';
		document.getElementById("palette").style.opacity = ".7";
	} else {
		document.getElementById("paletteOpacityButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">visibility</i><span>Change palette opacity</span></a>';
		document.getElementById("palette").style.opacity = "1";
	}
}

var paletteElements = paletteElements();
var paletteElementsReduced = paletteElementsReduced();
var actualElements = paletteElementsReduced;

sortPalette(false, 3, paletteElements);

paletteGraph.addCells(actualElements);

function paletteExtend() {
	extendedPalette = !extendedPalette;
	if (extendedPalette) {
		document.getElementById("paletteExtendButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">remove_circle</i><span>Reduce palette elements</span></a>';
		actualElements = paletteElements;
		if (horizontalPalette) {
			document.getElementById("palette").style.height = "201px";
		} else {
			document.getElementById("palette").style.width = "201px";
		}
	} else {
		document.getElementById("paletteExtendButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">add_circle</i><span>Extend palette elements</span></a>';
		actualElements = paletteElementsReduced;
		if (horizontalPalette) {
			document.getElementById("palette").style.height = "101px";
		} else {
			document.getElementById("palette").style.width = "101px";
		}
	}
	paletteGraph.clear();
	paletteGraph.addCells(actualElements);
}

function paletteRotate() {
	horizontalPalette = !horizontalPalette;
	sortPalette(horizontalPalette, 3, paletteElements);
	sortPalette(horizontalPalette, 3, paletteElementsReduced);
	paletteGraph.clear();
	paletteGraph.addCells(actualElements);
	/*if (horizontalPalette) {
	document.getElementById("palette").className = "palette-horizontal";
	} else {
	document.getElementById("palette").className = "palette-vertical";
	}
	document.getElementById("palette").style = null;
	document.getElementById("paletteTools").style = null;*/
	var actualWidth = document.getElementById("palette").style.width;
	var actualHeight = document.getElementById("palette").style.height;
	document.getElementById("palette").style.width = actualHeight;
	document.getElementById("palette").style.height = actualWidth;
}
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////
///////////////////////////TOOLBAR PARA LOS ELEMENTOS VISUALES
//////////////////////////////////////////////////////////////
/*paper.on('element:delete', function (elementView, evt) {
// Stop any further actions with the element view e.g. dragging
evt.stopPropagation();
if (confirm('Are you sure you want to delete this element?')) {
elementView.model.remove();
}
});*/

var actualElement = null;
var actualRenameElement = null;

var tools = $('<div class="toolbar"></div>');
tools.append('<div id="elementDeleteButton" class="tools tools-delete" onclick="elementDelete()"><a class="tooltips" href="#"><i class="material-icons">delete_forever</i><span>Remove the element</span></a></div>');
//tools.append('<div class="tools tools-clearlink">C</div>');
//tools.append('<div class="tools tools-newnext">N</div>');
tools.append('<div id="selectSuperEntity" class="tools tools-select-super-entity" onclick="selectSuperEntity(event)"><a class="tooltips" href="#"><i class="material-icons">expand_less</i><span>Define super entity</span></a></div>');
tools.append('<div id="compactClass" class="tools tools-compactClass" onmousedown="compactClass()"><a class="tooltips" href="#"><i class="material-icons">calendar_view_day</i><span>show/hide attributes and methods</span></a></div>');
tools.append('<div id="elementLinkButton" class="tools tools-link" onmousedown="elementLink(event)"><a class="tooltips" href="#"><i class="material-icons">trending_up</i><span>Connect to other element</span></a></div>');
tools.append('<div id="elementDuplicateButton" class="tools tools-duplicate" onclick="elementDuplicate()""><a class="tooltips" href="#"><i class="material-icons">file_copy</i><span>Duplicate the element</span></a></div>');
tools.css({
	display: 'none'
});


$('#paper').append(tools);
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////
///////////////MENU OPCIONES PARA EDITAR LOS ELEMENTOS
//////////////////////////////////////////////////////

$('body').append('<div id="menues"><div id=encabezadoMenu></div><div id=menuOpciones></div></div>');

$('#menues').focusin(function(){
    hideElementTools();
});

$('#encabezadoMenu').append('<h4>Opciones</h4><input id="btnMinimizeMenues" name="minimizeMenues" onclick=minimizeMenues() type="button" value="-">');
function minimizeMenues(){
	if ($('#btnMinimizeMenues').val() == '-'){
		$('#menuOpciones').css({
			display: 'none'
		});
		$('#menues').css({
			height: 35
		});
		$('#btnMinimizeMenues').val('+');
	}else{
		$('#menuOpciones').css({
			display: 'block'
		});
		$('#menues').css({
			height: 500
		});
		$('#btnMinimizeMenues').val('-');
	}
}

//para evitar que el puntero quede focuseando en un input cuando se selecciona un elemento del grafo. 
$('#paper').mouseup(function(evt){
    if (evt.target.tagName=='tspan'){
        $(document.activeElement).blur();
    }
});

//menu con opciones de clases
$('#menuOpciones').append('<div id="menuClass"></div>');
$('#menuClass').append('<div id="changeNameClass"><input id="classRenameInput" type="text"></div>');
$("#menuClass").append('<div id="listAttributes"></div>');
$('#menuClass').append('<input id="btnAddAttributeClass" name="addAttribute" onclick=addAttribute() type="button" value="add attr">');
$("#menuClass").append('<div id="listMethods"></div>');
$('#menuClass').append('<input id="btnAddMethodClass" name="addMethod" onclick=addMethod() type="button" value="add method">');
$('#menuClass').append('<input id="btnConfirmMenuClass" name="done" onclick=confirmMenuClass() type="button" value="done"><input id="btnCancelMenuClass" name="done" onclick=cancelMenuClass() type="button" value="cancel">');
$('#menuClass').css({
    display: 'none'
});

//menu con opciones de links
$('#menuOpciones').append('<div id="menuLink"></div>');
$('#menuLink').append('<div id="changeLabelLink"><label class="labelCardOrigin" for="labelCardOrigin">Cardinalidad origen</label><input class="labelCardOrigin" id="labelCardOrigin" name="labelCardOrigin" type="text"><label class="labelCardDestino" for="labelCardDestino">Cardinalidad Destino</label><input class="labelCardDestino" id="labelCardDestino" name="labelCardDestino" type="text"></div>');
$('#changeLabelLink').append('<label class="labelRoleOrigin" for="labelRoleOrigin">Role Origen</label><input class="labelRoleOrigin" id="labelRoleOrigin" name="labelRoleOrigin" type="text"><label class="labelRoleDestino" for="labelRoleDestino">Role Destino</label><input class="labelRoleDestino" id="labelRoleDestino" name="labelRoleDestino" type="text">');
$('#changeLabelLink').append('<label class="labelNameAssociation" for="labelNameAssociation">Nombre asociación</label><input class="labelNameAssociation" id="labelNameAssociation" name="labelNameAssociation" type="text">');
$('#menuLink').append('<input id="btnConfirmMenuLink" name="done" onclick=confirmMenuLink() type="button" value="done"><input id="btnCancelMenuLink" name="cancel" onclick=cancelMenuLink() type="button" value="cancel">');
$('#menuLink').css({
	display: 'none'
});

//menu con opciones de generalizacion
$('#menuOpciones').append('<div id="menuGeneralization"></div>');
$('#menuGeneralization').append('<label for="labelGeneralizationType">Tipo generalizacion</label><select id="selectGeneralizationType" name="selectGeneralizationType"><option value="">Default</option><option value="c">Covering</option><option value="d">disjoint</option><option value="c/d">covering/disjoint</option>');
$('#menuGeneralization').append('<input id="btnConfirmMenuGeneralization" name="done" onclick=confirmMenuGeneralization() type="button" value="done"><input id="btnCancelMenuGeneralization" name="cancel" onclick=cancelMenuGeneralization() type="button" value="cancel">');
$('#menuGeneralization').css({
    display: 'none'
});


disableTool('#elementLinkButton');
disableTool('#elementDuplicateButton');
$('#elementLinkButton').css({
    display: "none"
});

$('#elementDuplicateButton').css({
    display: "none"
});

function disableTool(tool) {
	$(tool).css({
		'background': '#787575',
		'pointer-events': 'none',
	});
}

//para mostrar las herramientas cuando se hace click sobre un elemento especifico
paper.on('element:pointerclick', function (cellView, evt) {
	actualElement = cellView;
    if (getType(cellView.model)=='Class'){
        showElementToolsClass(cellView);
    }else if (getType(cellView.model)=='Inheritance'){
        showElementToolsGeneralization(cellView);
    }else if (getType(cellView.model)=='Nary'){
        showElementToolsNary(cellView);
    }
});

/*para mostrar el menu de opciones cuando se hace doble click sobre un elemento cell.*/
paper.on('cell:pointerdblclick', function (cellView, evt) {
	//eliminar cualquier menu abierto
    ocultarMenues();
    if (getType(cellView.model) == 'Class'){
        menuClass(cellView);
    }else if (getType(cellView.model) == 'Inheritance'){
        menuGeneralization(cellView);
    }else if (getType(cellView.model) == 'Link'){
        menuLink(cellView);
    }
});

//oculta cualquier div dentro del div menuOpciones. Usado para ocultar los menus link, class, etc.
function ocultarMenues(){
	var mns = $('#menuOpciones').children();
	for (var i = mns.length - 1; i >= 0; i--) {
		if (mns[i].style.display=='block'){
			mns[i].style.display = 'none';
		}
	}
}

$('body').on('keydown', function (e) {
	if (e.key === "Delete") {
		if (actualElement != null) {
			elementDelete();
		}
	}
});


//Eventos para ocultar las herramientas visuales de los elementos html
paper.on('element:pointermove', function (cellView, evt) {
	hideElementTools();
});

$('#paper').on('mousewheel DOMMouseScroll', function (evt) {
	hideElementTools();
});

$('#elementNameText').focusout(function () {
	hideElementTools();
});

//muestra las herramientas para las generalizaciones
function showElementToolsGeneralization(figure) {
    var pos = paper.localToClientPoint(figure.model.attributes.position);
    //var screenPos = paper.localToClientPoint(pos);
    tools.width($("#"+figure.model.id).width());
    tools.height($("#"+figure.model.id).height());
    /*tools.width(figure.model.attributes.size.width * paper.scale().sx + $('.tools').width() * 2);
    tools.height(figure.model.attributes.size.height * paper.scale().sy + $('.tools').height() * 2);*/
    tools.attr('elementid', figure.model.id);
    tools.css({
        top: pos.y - 25,
        left: pos.x - 20,
        display: 'block'
    });

    //oculta/muestra la opcion de seleccionar super entity
    $('#selectSuperEntity').css({
        display: 'block'
    });   

    $('#compactClass').css({
        display: "none"
    });

}

//recibe una uml.class para mostrar el elementTools
function showElementToolsClass(cls) {
    var pos = paper.localToClientPoint(cls.model.attributes.position);
    //var screenPos = paper.localToClientPoint(pos);
    tools.width($("#"+cls.id).width()+45);
    tools.height($("#"+cls.id).height()+20);
    tools.attr('elementid', cls.model.id);
    tools.css({
        top: pos.y-20,
        left: pos.x-20,
        display: 'block'
    });

    $('#compactClass').css({
        display: "block"
    });
    //oculta la opcion de seleccionar super entity
    $('#selectSuperEntity').css({
        display: 'none'
    });

}
//muestra las herramientas de Nary. (solo borrar element)
function showElementToolsNary(cls) {
    var pos = paper.localToClientPoint(cls.model.attributes.position);
    //var screenPos = paper.localToClientPoint(pos);
    tools.width($("#"+cls.id).width()+45);
    tools.height($("#"+cls.id).height()+20);
    tools.attr('elementid', cls.model.id);
    tools.css({
        top: pos.y-20,
        left: pos.x-20,
        display: 'block'
    });

    $('#compactClass').css({
        display: "none"
    });
    //oculta la opcion de seleccionar super entity
    $('#selectSuperEntity').css({
        display: 'none'
    });

}

//oculta las herramientas de los elementos
function hideElementTools() {
	tools.css('display', 'none');
	actualElement = null;
}

function elementDelete() {
	var cell = graphMain.getCell(tools.attr('elementid'));
	cell.remove();
	//actualElement.remove();
	hideElementTools();
}

//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//////////////////////////////////////////
/////////////////// MENU PARA LOS LINKS
//////////////////////////////////////////
//menuLink gestiona las funcionalidades de un link
var actualLink = null;
function menuLink(link) {
    hideElementTools();
    actualLink = link;
    if (link.model.attr('customAttr/type')=='binaryAssociation'){
        $('.labelCardOrigin').val(link.model.attributes.labels[0].attrs.text.text);
        $('.labelCardDestino').val(link.model.attributes.labels[1].attrs.text.text);
        $('.labelRoleOrigin').val(link.model.attributes.labels[2].attrs.text.text);
        $('.labelRoleDestino').val(link.model.attributes.labels[3].attrs.text.text);
        $('.labelNameAssociation').val(link.model.attributes.labels[4].attrs.text.text);
        $('.labelCardDestino').css({display:"block"});
        $('.labelRoleDestino').css({display:"block"});
        $('.labelNameAssociation').css({display:"block"});
        $("#menuLink").css({display:"block"});
    }else if (link.model.attr('customAttr/type')=='class2rombo'){
        $('.labelCardOrigin').val(link.model.attributes.labels[0].attrs.text.text);
        $('.labelRoleOrigin').val(link.model.attributes.labels[1].attrs.text.text);
        $('.labelCardDestino').css({display:"none"});
        $('.labelRoleDestino').css({display:"none"});
        $('.labelNameAssociation').css({display:"none"});
        $("#menuLink").css({display:"block"});
    }
}

//para setear los labels de un link una vez presionado el botton en #changeLabel
function confirmMenuLink(){
    if (actualLink.model.attr('customAttr/type')=='binaryAssociation'){
        changeLabelCardinalityBinaryAssociation();
        changeLabelRoleBinaryAssociation();
        changeNameAssociation();
    }else{
        changeLabelCardinalityClassAssociation();
        changeLabelRoleClassAssociation();
    }

    actualLink = null;
    $("#menuLink").css({display:"none"});
}

function changeLabelCardinalityBinaryAssociation(){
    if (actualLink!=null){
        if ($('#labelCardOrigin').val()!='undefined'){
            actualLink.model.label(0, {
                attrs: {
                    text: {
                        text: $('#labelCardOrigin').val()
                    }
                }
            });
        }
        if ($('#labelCardDestino').val()!='undefined'){
            actualLink.model.label(1, {
                attrs: {
                    text: {
                        text: $('#labelCardDestino').val()
                    }
                }
            });
        }
    }
}

function changeLabelRoleBinaryAssociation(){
    if (actualLink!=null){
        if ($('#labelRoleOrigin').val()!='undefined'){
            actualLink.model.label(2, {
                attrs: {
                    text: {
                        text: $('#labelRoleOrigin').val()
                    }
                }
            });
        }
        if ($('#labelRoleDestino').val()!='undefined'){
            actualLink.model.label(3, {
                attrs: {
                    text: {
                        text: $('#labelRoleDestino').val()
                    }
                }
            });
        }
    }
}

function changeLabelCardinalityClassAssociation(){
    if (actualLink!=null){
        if ($('#labelCardOrigin').val()!='undefined'){
            actualLink.model.label(0, {
                attrs: {
                    text: {
                        text: $('#labelCardOrigin').val()
                    }
                }
            });
        }
    }
}

function changeLabelRoleClassAssociation(){
    if (actualLink!=null){
        if ($('#labelRoleOrigin').val()!='undefined'){
            actualLink.model.label(1, {
                attrs: {
                    text: {
                        text: $('#labelRoleOrigin').val()
                    }
                }
            });
        }
    }
}

function changeNameAssociation(){
    if (actualLink!=null){
        if ($('#labelNameAssociation').val()!='undefined'){
            actualLink.model.label(4, {
                attrs: {
                    text: {
                        text: $('#labelNameAssociation').val()
                    }
                }
            });
        }
    }
}

function cancelMenuLink(){
    actualLink = null;
    $("#menuLink").css({display:"none"});
}

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////

/////////////////////////////////////////
/////////////////// MENU PARA LAS CLASES
/////////////////////////////////////////
//menuClass gestiona las funcionalidades de una class
var actualClass = null;
function menuClass(cls) {
    hideElementTools();
    actualClass = cls;
    //setear el nombre de la clase en el textbox
    $('#classRenameInput').val(actualClass.model.attr('.uml-class-name-text/text'));
    //limpiar los atributos y crear los pertenecientes a esta clase
    ($("#listAttributes")).empty();
    if (actualClass.model.attributes.attributes.length!=0){
        //muestra todos los atributos
        var ats = actualClass.model.attributes.attributes;
        for (var i = 0; i <= ats.length - 1; i++) {
            $("#listAttributes").append('<div id="atts_'+actualClass.id+'_'+i+'"><input class="attributeClass" type="text" value="'+ats[i]+'"><input class="btnDeleteInputClass" type="button" value="-" onclick="deleteInput(atts_'+actualClass.id+'_'+i+')"></div>');
        }
    }
    //limpiar los metodos y crear los pertenecientes a esta clase
    ($("#listMethods")).empty();
    if (actualClass.model.attributes.methods.length!=0){
        //muestra todos los atributos
        var ats = actualClass.model.attributes.methods;
        for (var i = 0; i <= ats.length - 1; i++) {
            $("#listMethods").append('<div id="mts_'+actualClass.id+'_'+i+'"><input class="methodClass" type="text" value="'+ats[i]+'"><input class="btnDeleteInputClass" type="button" value="-" onclick="deleteInput(mts_'+actualClass.id+'_'+i+')"></div>');
        }
    }
    //mostrar el menu de clases
    $("#menuClass").css({display:"block"});
    $('#classRenameInput').focus();
}

//btn agregar atributo
function addAttribute(cls){
    var childs = ($("#listAttributes")).children().length+1;
    $("#listAttributes").append('<div id="atts_'+actualClass.id+'_'+childs+'"><input class="attributeClass" type="text"><input class="btnDeleteInputClass" type="button" value="-" onclick="deleteInput(atts_'+actualClass.id+'_'+childs+')"></div>');

}

//btn agregar method
function addMethod(cls){
    var childs = ($("#listMethods")).children().length+1;
    $("#listMethods").append('<div id="mts_'+actualClass.id+'_'+childs+'"><input class="methodClass" type="text"><input class="btnDeleteInputClass" type="button" value="-" onclick="deleteInput(mts_'+actualClass.id+'_'+childs+')"></div>');
}

//btn eliminar objeto html
function deleteInput(objectID){
    $("#"+objectID.id).remove();
}

//cambia los atributos de una clase
function changeAttributes(){
    var i = 0;
    var ats = [];
    var childs = ($("#listAttributes")).children();
    for (var i = 0; i <= childs.length - 1; i++) {
        ats[i]=$(childs[i]).children()[0].value;
        childs[i].remove();
    }
    actualClass.model.attributes.attributes = ats;
}

//cambia los metodos de una clase
function changeMethods(){
    var i = 0;
    var mts = [];
    var childs = ($("#listMethods")).children();
    for (var i = 0; i <= childs.length - 1; i++) {
        mts.push($(childs[i]).children()[0].value);
        childs[i].remove();
    }
    actualClass.model.attributes.methods = mts;
}

//cambia el nombre de una clase
function changeNameClass(){
    if (actualClass!=null){
        if ($('#classRenameInput').val()!='undefined'){
            actualClass.model.attributes.name = $('#classRenameInput').val();
        }
    }
}

//cambia el tamaño de la clase segun el contenido
function resizeClass(cls){
    var w = 0;
    var ats = cls.model.attributes.attributes;
    var mts = cls.model.attributes.methods;
    //busca la palabra mas larga entre los atributos
    for (var i = ats.length - 1; i >= 0; i--) {
        if (ats[i].length>w){
            w = ats[i].length;
        }
    } //busca la palabra mas larga entre los atributos y los metodos
    for (var i = mts.length - 1; i >= 0; i--) {
        if (mts[i].length>w){
            w = mts[i].length;
        }
    }
    //compara el tamaño del nombre de la clase con el w. la clase vale +20 porque está centrada
    if (cls.model.attributes.name.length+20>w){
        w = cls.model.attributes.name.length;
    }

    //como minimo tiene que tener 100px;
    if (w*7<100){
        w = 15;
    }
    var h = ats.length+mts.length+10;
    cls.model.resize(w*7,h*8);
}

//cambia el tamaño de la clase para ocultar sus metodos y atributos
function compactClass(){
    if (!actualElement.model.attr('customAttr/isCompact')){
        var w = actualElement.model.attributes.name.length;

        //como minimo tiene que tener 100px;
        if (w*7<100){
            w = 15;
        }
        actualElement.model.attr('.uml-class-methods-text/text','');
        actualElement.model.attr('.uml-class-attrs-text/text','');
        actualElement.model.attr('.uml-class-methods-rect').height = 0;
        actualElement.model.attr('.uml-class-attrs-rect').height  = 0;
        actualElement.model.attr('.uml-class-name-rect').height = 40;
        //actualElement.model.resize(w*7,75);
        actualElement.model.attr('customAttr/isCompact',true);
        $('.toolbar').css({
            height: 50
        });
    }else{
        //resizeClass(actualElement);
        actualElement.model.trigger('change:name');
        actualElement.model.attr('customAttr/isCompact',false);
        var h = actualElement.model.attr('.uml-class-attrs-rect').height + actualElement.model.attr('.uml-class-methods-rect').height + actualElement.model.attr('.uml-class-name-rect').height;
        $('.toolbar').css({
            height: h
        });
    }
}



//confirma todos los cambios a la class. (desde interfaz con boton done).
function confirmMenuClass(){
    changeNameClass();
    changeAttributes();
    changeMethods();
     //lanza un evento para que actualice los datos del objeto visual class.
     actualClass.model.trigger('change:name');
     resizeClass(actualClass);
    //vuelve a su tamaño por lo que ya no esta compacta.
    actualClass.model.attr('customAttr/isCompact',false);
    actualClass = null;
    $("#menuClass").css({display:"none"});
}

//cancela los cambios a una clase.
function cancelMenuClass(){
    var i = 0;
    ($("#listAttributes")).empty();
    actualClass = null;
    $("#menuClass").css({display:"none"});
}
//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////


//////////////////////////////////////////////////
/////////////////// MENU PARA LAS GENERALIZACIONES
//////////////////////////////////////////////////
actualGeneralization = null;
function menuGeneralization(cellView){
    hideElementTools();
    actualGeneralization = cellView;
    //setear el valor que tiene la generalizacion en el input select
    $("#selectGeneralizationType").val(actualGeneralization.model.attr('text/text'));
    $("#menuGeneralization").css({display:"block"});
}

function confirmMenuGeneralization(){
    actualGeneralization.model.attr('text/text',$("#selectGeneralizationType").val());
    actualGeneralization.model.attr('customAttr/type',$("#selectGeneralizationType").val());
    actualGeneralization = null;
    $("#menuGeneralization").css({display:"none"});
}

function cancelMenuGeneralization(){
    actualGeneralization = null;
    $("#menuGeneralization").css({display:"none"});
}
//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////

//para seleccionar la superClass en la generalizacion
var ISASelected = null;
function selectSuperEntity(event) {
    ISASelected = actualElement.model;
    var links = graphMain.getConnectedLinks(ISASelected);
    for (var i = 0; i < links.length; i++) {
        var elm = (links[i].source().id != ISASelected.id ? graphMain.getCell(links[i].source().id) : graphMain.getCell(links[i].target().id));
        if (getType(elm) == 'Class') {
            markElement(elm,'selectGeneralizationType');
        }
    }
}

//setear la superClass de la herencia y agregar las subClasses y superClasses como atributos
paper.on('cell:pointerdown blank:pointerdown', function (elementView, evt) {
    var linkToSuperClass = null;
    if (ISASelected != null) {
        //agregar elementView al atributo superClasses de ISASelected
        ISASelected.attr('customAttr/superClasses',[elementView.model]);
        var links = graphMain.getConnectedLinks(ISASelected);
        var newSubClasses = [];
        //saca la marca a las entidades y verifica que se seleccionó alguna de ellas
        for (var i = 0; i < links.length; i++) {
            var link = links[i];
            var elm = (link.source().id != ISASelected.id ? graphMain.getCell(link.source().id) : graphMain.getCell(link.target().id));
            if (getType(elm) == 'Class') {
                unmarkElement(elm);
                if (elementView.model != null && elm.id == elementView.model.id) {
                    linkToSuperClass = link;
                }
                if (elementView.model != null && elementView.model.id!=elm.id && ISASelected.id!=elm.id){
                    //agregar elm al atributo subClasses de ISASelected
                    newSubClasses.push(elm);
                }
            }
        }

        ISASelected.attributes.attrs.customAttr.subClasses = newSubClasses;
        //vuelve a recorrer los link para marcar direction los links
        if (linkToSuperClass != null) {
            for (var i = 0; i < links.length; i++) {
                var link = links[i];
                setDirection(link,true);
                if (link == linkToSuperClass) {
                    setInheritance(link,true);
                } else {
                    setInheritance(link,false);
                }
            }
        }
        ISASelected = null;
        hideElementTools();
    }
});
//setea la direccion del link (util en el diagrama e-r)
function setDirection(link,value) {
    var marker = getType(graphMain.getCell(link.source().id)) == 'Class' ? 'sourceMarker' : 'targetMarker';
    link.attr('customAttr/direction',value);
}

//setea el attr inheritance y cambia el formato de las flechas
function setInheritance(link,value) {
    var marker = getType(graphMain.getCell(link.source().id)) == 'Class' ? 'sourceMarker' : 'targetMarker';
    link.attr('customAttr/inheritance',value);
    if (value) {
        //link.attr('line/'+marker+'/d','M 10 -5 0 0 10 5 Z');
        link.attr('line/'+marker+'/d','m 0 0 T 30 -15 q 0 0 0 30 z');
        link.attr('line/'+marker+'/type','path');
        link.attr('line/'+marker+'/stroke-width',2);
        link.attr('line/'+marker+'/fill','white');
    } else {
        link.attr('line/'+marker+'/d','');
    }
}

//crea un conector
var createLink = function () {
	//para agregar controles en conectores
	var verticesTool = new joint.linkTools.Vertices();
	var segmentsTool = new joint.linkTools.Segments();
	var sourceAnchorTool = new joint.linkTools.SourceAnchor();
	var targetAnchorTool = new joint.linkTools.TargetAnchor();
	var boundaryTool = new joint.linkTools.Boundary();
	var removeButton = new joint.linkTools.Remove({
       distance: 20
   });

	var toolsView = new joint.dia.ToolsView({
       tools: [
       verticesTool, segmentsTool,
				//sourceArrowheadTool, targetArrowheadTool,
				sourceAnchorTool, targetAnchorTool,
				boundaryTool, removeButton
				//totalButton, cardinalityButton
               ]
           });

	var myLink = new joint.shapes.standard.Link();

	myLink.attr({
		line: {
			stroke: 'black',
			strokeWidth: 2,
			sourceMarker: {},
			targetMarker: {
				'd': ''
			}
		},
		customAttr: {
			total: false,
			direction: false,
			inheritance: false,
            type: 'generic'
        }
    });

	/*myLink.connector('jumpover', {
		size: 10
	});*/

	var link = myLink.addTo(graphMain);
	var linkView = myLink.findView(paper);
	linkView.addTools(toolsView);
	return link;
};

//crea un conector especifico para las relaciones binarias entre clases
var createLinkClass = function (type) {
	//para agregar controles en conectores
	//var editLabelButton = new joint.linkTools.EditLabelButton();
	var verticesTool = new joint.linkTools.Vertices({
        focusOpacity: 0.5,
        snapRadius: 5,
    });
	var segmentsTool = new joint.linkTools.Segments();
	//var sourceArrowheadTool = new joint.linkTools.SourceArrowhead();
	//var targetArrowheadTool = new joint.linkTools.TargetArrowhead();
	var sourceAnchorTool = new joint.linkTools.SourceAnchor();
	var targetAnchorTool = new joint.linkTools.TargetAnchor();
	var boundaryTool = new joint.linkTools.Boundary();
	var removeButton = new joint.linkTools.Remove({
       distance: 20
   });
	var toolsView = new joint.dia.ToolsView({
       tools: [
       new joint.linkTools.Vertices({
                    snapRadius: 0,
                    redundancyRemoval: false,
                    vertexAdding: false,
                    mouseenter: function(){

                    }
                }),
       //verticesTool, segmentsTool,
				//sourceArrowheadTool, targetArrowheadTool,
				//sourceAnchorTool, targetAnchorTool,
				removeButton,
				//editLabelButton
               ]
           });

	var myLink = new joint.shapes.standard.Link();

    myLink.attr({
        connector: { name: 'smooth' },
        line: {
            stroke: 'black',
            strokeWidth: 2,
            sourceMarker: {},
            targetMarker: {
                'd': ''
            },
            vertexMarker: {
                'type': 'circle',
                'r': 5,
                'stroke-width': 2,
                'fill': 'black'
            }
        },
        customAttr: {
            total: false,
            direction: false,
            inheritance: false,
            type: type,
        },
    });

    /*myLink.connector('jumpover', {
      size: 10
  });*/

  var link = myLink.addTo(graphMain);
  var linkView = myLink.findView(paper);
  linkView.addTools(toolsView);
  linkView.hideTools();
  console.log(linkView);
  return link;
};

//crea un conector especifico (linea punteada) para conectar un rombo con su clase.
var createLinkClassDashed = function () {
    //para agregar controles en conectores
    //var editLabelButton = new joint.linkTools.EditLabelButton();
    var verticesTool = new joint.linkTools.Vertices({
        focusOpacity: 0.5,
        snapRadius: 5,
    });
    var segmentsTool = new joint.linkTools.Segments();
    //var sourceArrowheadTool = new joint.linkTools.SourceArrowhead();
    //var targetArrowheadTool = new joint.linkTools.TargetArrowhead();
    var sourceAnchorTool = new joint.linkTools.SourceAnchor();
    var targetAnchorTool = new joint.linkTools.TargetAnchor();
    var boundaryTool = new joint.linkTools.Boundary();
    var removeButton = new joint.linkTools.Remove({
        distance: 20
    });

    var toolsView = new joint.dia.ToolsView({
        tools: [
        verticesTool, segmentsTool,
                //sourceArrowheadTool, targetArrowheadTool,
                sourceAnchorTool, targetAnchorTool,
                boundaryTool, removeButton,
                //editLabelButton
                ]
            });

    var myLink = new joint.shapes.standard.Link();

    myLink.attr({
        line: {
            stroke: 'black',
            strokeWidth: 2,
            strokeDasharray: '5 5',
            sourceMarker: {},
            targetMarker: {
                'd': ''
            }
        },
        customAttr: {
            total: false,
            direction: false,
            inheritance: false,
            type: 'classAssociation'
        }
    });

    myLink.connector('jumpover', {
        size: 10
    });

    var link = myLink.addTo(graphMain);
    var linkView = myLink.findView(paper);
    linkView.addTools(toolsView);
    linkView.hideTools();

    return link;
};

//crea un conector especifico (linea punteada) para conectar dos roles con links.
function createLinkProperty2Property() {
    var myLink = new joint.shapes.standard.Link();

    myLink.attr({
        line: {
            stroke: 'gray',
            strokeWidth: 2,
            strokeDasharray: '5 5',
            sourceMarker: {},
            targetMarker: {
                'd': 'M 15 -5 L 15 5 L 0 0 z'
            }
        },
    });

    return myLink;
}

paper.on('link:mouseenter', function (linkView, evt) {
    //console.log(linkView.sourcePoint);
    //para que no molesten las opciones de la flecha cuando se selecciona superEntity
    if (ISASelected == null) {
        linkView.showTools();
    }
    //hacemos visible el port del link
    if (linkView.model.attributes.attrs != undefined && linkView.model.attributes.attrs.customAttr!=undefined){
        if (linkView.model.attributes.attrs.customAttr.port != undefined){
            linkView.model.attributes.attrs.customAttr.port.attr('./display', 'block');
        }
    }

});

paper.on('link:pointerclick', function (linkView, evt, x, y) {
    linkView.addVertex(x, y);
});

paper.on('blank:mouseover', function() {
    paper.hideTools();
});

paper.on('link:mouseleave', function(linkView, evt) {
    linkView.hideTools();

    //hacemos invisible el port del link
    if (linkView.model.attributes.attrs != undefined && linkView.model.attributes.attrs.customAttr!=undefined){
        if (linkView.model.attributes.attrs.customAttr.port != undefined){
            var relativePoint = paper.clientToLocalPoint(evt.clientX, evt.clientY);
            if (!linkView.model.attributes.attrs.customAttr.port.getBBox().containsPoint(relativePoint)){
                linkView.model.attributes.attrs.customAttr.port.attr('./display', 'none');
            }
        }
    }
});

//ocultar el port cuando el mouse sale desde el rectangulo del port y no desde el link.
paper.on('cell:mouseleave', function(cellView, evt) {
    if (cellView.model.getParentCell()!=null){
        if (getType(cellView.model.getParentCell())=='Link') {
            var linkModel = cellView.model.getParentCell();
            if (linkModel.attributes.attrs != undefined && linkModel.attributes.attrs.customAttr!=undefined){
                if (linkModel.attributes.attrs.customAttr.port != undefined){
                    linkModel.attributes.attrs.customAttr.port.attr('./display', 'none');
                }
            }
        }
    }
});

var connectLink = function (myLink, elm1, elm2) {
	myLink.source({
		id: elm1.id
	});
	myLink.target({
		id: elm2.id
	});
};

//devuelve los labels para agregar a un conector
var createLabel = function (txt) {
	var label = {
		attrs: {
			text: {
				text: txt,
				fill: 'black',
				fontSize: 15
			},
			rect: {
				fill: 'white'
			}
		},
		position: {
			distance: 0.5,
			offset: 15
		}
	}
	return label;
};

//devuelve los labels para agregar a un conector. Recibe posicion
var createLabelWithPosition = function (txt, position) {
	var dis = position.distance;
	var off = position.offset;
	var label = {
		attrs: {
			text: {
				text: txt,
				fill: 'black',
				fontSize: 12
			},
			rect: {
				fill: 'white'
			}
		},
		position: {
			distance: dis,
			offset: off
		}
	}
	return label;
};

function addPortToLink(link){
    /*Port para hacer subClass link*/
    /*Se busca la posicion absoluta de inicio y fin del link en el paper. Para esto se obtienen sus X,Y inicial y final.
    Luego se calcula un vector del 25% de longitud del link y se posiciona el port usando este vector + la posicion del source del link.*/
    var posData = paper.findViewByModel(link).metrics.data.split(" ");
    positionAbsoluteX1 = posData[1];
    positionAbsoluteY1 = posData[2];
    positionAbsoluteX2 = posData[4];
    positionAbsoluteY2 = posData[5];
    var mediaX = (positionAbsoluteX2 - positionAbsoluteX1)/4;
    var mediaY = (positionAbsoluteY2 - positionAbsoluteY1)/4;
    var positionResX = (Number(positionAbsoluteX1)+Number(mediaX))-35;
    var positionResY = (Number(positionAbsoluteY1)+Number(mediaY))-35;
    var rect = new joint.shapes.basic.Rect({
        position: { x: positionResX, y: positionResY },
        attrs: {
            rect: {
                fill: 'none',
                'ref-x': -12.5,
                stroke: 'none'
            },
            root: { magnet: false },
        },
        size: { width: 30, height: 30 },
        ports: {
            groups: {
                'link2link': {
                    attrs: {
                        circle: {
                            fill: 'white',
                            magnet: true,
                            r: 7,
                            class: "circlePort",
                        },
                    },
                },
            },
            items:[ { group: 'link2link' }],
        }
    });
    graphMain.addCell(rect);
    //por defecto el rect que contiene al port es invisible
    rect.attr('./display', 'none');
    //se agrega este port como atributo del link para poder reconocer las relaciones de este link con otros links (jerarquia de links).
    link.attr('customAttr/port',rect);
    //embeber port en link. Asi cuando se elimina el link se elimina el port.
    link.embed(rect);
    rect.toFront();

}


// para crear links arrastrando una elemento sobre otro
var dataElement;
paper.on({
	'element:pointerdown': function (elementView, evt) {
		evt.data = elementView.model.position();
		dataElement = evt;
	},

	'element:pointerup': function (elementView, evt, x, y) {
		var coordinates = new g.Point(x, y);
		var elementAbove = elementView.model;
		var elementBelow = this.model.findModelsFromPoint(coordinates).find(function (el) {
            return (el.id !== elementAbove.id);
        });

		// If the two elements are connected already, don't
		// connect them again (this is application-specific though).
		if (elementBelow && graphMain.getNeighbors(elementBelow).indexOf(elementAbove) === -1 && canConnect(elementBelow, elementAbove)) {

			// Move the element to the position before dragging.
			elementAbove.position(dataElement.data.x, dataElement.data.y);

			// Create a connection between elements.
			// var link = new joint.shapes.standard.Link();
			// link.source(elementAbove);
			// link.target(elementBelow);
			// link.addTo(graphMain);
			if (getType(elementBelow) != 'Attribute' && getType(elementAbove) != 'Attribute' &&
				getType(elementBelow) != 'Generalization' && getType(elementAbove) != 'Generalization') {
				//si es una relacion entre 2 clases, se agregan los labels para la cardinalidad
            if (getType(elementBelow)=='Class' && getType(elementAbove) == 'Class'){
             var newLink = createLinkClass('binaryAssociation');
             connectLink(newLink, elementBelow, elementAbove);
             setAttrsLinkClass2Class(newLink);
               //agregar port al link
               addPortToLink(newLink);
           }else{
             var newLink = createLink();
             connectLink(newLink, elementBelow, elementAbove);
         }
     }

 }else{
    if (elementBelow != undefined){           
                // Move the element to the position before dragging.
                elementAbove.position(dataElement.data.x, dataElement.data.y);
            }
        }
    }
});

//agrega los labels y el name a un link entre dos clases
function setAttrsLinkClass2Class(link){
    var positionLabelCardOrigin = {
        distance: 0.85,
        offset: 15
    }
    var positionLabelCardDestino = {
        distance: 0.15,
        offset: 15
    }                  
    var positionLabelRoleOrigin = {
        distance: 0.70,
        offset: -15
    }
    var positionLabelRoleDestino = {
        distance: 0.30,
        offset: -15
    }
    var positionLabelName = {
        distance: 0.50,
        offset: 0
    }
    link.appendLabel(createLabelWithPosition('1..6', positionLabelCardOrigin));
    link.appendLabel(createLabelWithPosition('1..5', positionLabelCardDestino));
    link.appendLabel(createLabelWithPosition('role1', positionLabelRoleOrigin));
    link.appendLabel(createLabelWithPosition('role2', positionLabelRoleDestino));
    link.appendLabel(createLabelWithPosition('aName', positionLabelName));
}

//agrega los labels a un link entre una clase y un rombo
function setAttrsLinkClass2Rombo(link){
    var positionLabelCardOrigin = {
        distance: 0.10,
        offset: 15
    }              
    var positionLabelRoleOrigin = {
        distance: 0.35,
        offset: -15
    }
    link.appendLabel(createLabelWithPosition('1..6', positionLabelCardOrigin));
    link.appendLabel(createLabelWithPosition('role1', positionLabelRoleOrigin));
}


//para dragear la pagina
paper.on('blank:pointerdown', function (event, x, y) {
	hideElementTools();
	//dragStartPositionMain = { x: x, y: y};
	var scale = paper.scale();
	var dragStartPositionMain = {
		x: x * scale.sx,
		y: y * scale.sy
	};

    $("#paper").mousemove(function (event) {
        if (dragStartPositionMain != null) {
        //console.log("mousemove");
        paper.translate(
            event.offsetX - dragStartPositionMain.x,
            event.offsetY - dragStartPositionMain.y);
    }
});

    paper.on('cell:pointerup blank:pointerup', function (cellView, x, y) {
        dragStartPositionMain = null;
    });
});




$('#paper').on('mousewheel DOMMouseScroll', function (evt, x, y) {
	evt.preventDefault();
	var delta = Math.max(-1, Math.min(1, (evt.originalEvent.wheelDelta || -evt.originalEvent.detail)));
	var newScale = paper.scale().sx + delta / 50;
    if (newScale > 0.4 && newScale < 2) {
		//paper.translate(0, 0);
		paper.scale(newScale, newScale); //, p.x, p.y);
	}
});

/*para que los div se acomoden al tamaño de la ventana*/
$(window).resize(function () {
	//$("#palette").width($(window).width()*0.25);
	//$("#palette").height($(window).height());
	$("#paper").width($(window).width());
	$("#paper").height($(window).height());
});


var errorList = [];


function check() {
	cleanInference();
	checkSintaxis();
	var req = checkSemantica();
	var rr;
	// Gestor del evento que indica el final de la petición (la respuesta se ha recibido)
	req.addEventListener("load", function() {
	// La petición ha tenido éxito
	if (req.status >= 200 && req.status < 400) {
	    //console.log(req.responseText);
	    rr = req.responseText.toString();
      if (rr.length == 4){
       alert('Modelo consistente');
   } else {
			//quitar corchetes
			var classes = JSON.parse(rr);
			var i;
			for (i = 0; i<classes.length;i++){
				//console.log(classes[i]);
				var elem = graphMain.getCells();
				var j;
				for (j = 0; j<elem.length;j++){
					//console.log(elem[j].attr('text/text'));
					//console.log(classes[i]);
					//console.log(classes[i].toUpperCase());
					//console.log(elem[j].attr('text/text').toUpperCase());
                    if (getType(elem[j])!='Error'){
                     if (elem[j].attr('text/text').toUpperCase() == classes[i].toUpperCase()){
                      markElement(elem[j],'semantico');
                  }
              }
          }
      }
  }
} else {
	  	// Se muestran informaciones sobre el problema ocasionado durante el tratamiento de la petición
     console.error(req.status + " " + req.statusText);
 }
});
}

//para consultar a omelet
function checkSemantica() {
	// Creación de la petición HTTP
	var req = new XMLHttpRequest();
	// Petición HTTP POST asíncrona si el tercer parámetro es "true" o no se especifica
	req.open("POST", "http://localhost:3000/", true);
	// Envío de la petición
	var q = exportJSON();
	console.log(q);
	var query = '{"type": "check","data": '+q+'}';
	req.send(query);
	// Gestor del evento que indica que la petición no ha podido llegar al servidor
	req.addEventListener("error", function(){
	  console.error("Error de red"); // Error de conexión
	});
	return req;
}


//exportar json
function exportJSON() {
    var allElement = getAllElement();
    var classes = allElement[0];
    var inheritances = allElement[1];
    var assocWithClass = allElement[2];
    var associations = [];
    var elements = graphMain.getElements();
    var links = graphMain.getLinks();
    for (var i = 0; i < links.length; i++) {
        var link = links[i];
        if (link.attr('customAttr/type')=='binaryAssociation' || link.attr('customAttr/type')=='class2rombo'){
            var element1 = graphMain.getCell(link.source().id);
            var element2 = graphMain.getCell(link.target().id);
            var infoLink = getInfoLink(link);
            var newLink = {
                info: infoLink,
                source: element1,
                target: element2,
                type: link.attr('customAttr/type')
            }
            associations.push(newLink);
        }
    }
    var json = {
        classes: classes,
        inheritances: inheritances,
        associationWithClass: assocWithClass,
        associations: associations
    }
    console.log(json);
}

function getAllElement() {
    //retorna todos los elementos en un arreglo con la forma [entidades,relaciones,atributos,herencias,conectores]
    var inheritances = [];
    var classes = [];
    var assocWithClass = [];
    var elements = graphMain.getElements();

    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var type = getSpecificType(element);
        var name = element.attr('text/text');
        //name = name.replace('\n',"\\n");
        var cid = element.cid;
        var id = cid.match(/\d+/g)[0];
        var numID = new Number(id);
        var dataType = element.attr('customAttr/type');
        switch (type) {
            case "Class":
            name = element.attr('.uml-class-name-text/text');
                //name = name.replace('\n',"\\n");
                var newClass = {
                    name: name,
                    id: cid,
                    attributes: element.attributes.attributes,
                    methods: element.attributes.methods,
                }
                classes.push(newClass);
                break;
                case "Nary":
                var cls = getClassAssociation(element);
                var newAssoc = {
                    id: cid,
                    classAssociation: cls
                }
                assocWithClass.push(newAssoc);
                break;
                case "Inheritance":
                var newInh = {
                    id: cid,
                    type: name,
                    superClasses: element.attr('customAttr/superClasses'),
                    subClasses: element.attr('customAttr/subClasses')
                }
                inheritances.push(newInh);
                break;
            }
        }
        return [classes,inheritances,assocWithClass];
    }

    function getClassAssociation(assoc){
        var links = graphMain.getLinks();
        var classAssoc = null;
        for (var i = 0; i < links.length; i++) {
            if (links[i].attr('customAttr/type')=='classAssociation'){
                if (links[i].source().id==assoc.id) {
                    classAssoc = graphMain.getCell(links[i].target().id);
                    break;
                }else if(links[i].target().id==assoc.id){
                    classAssoc = graphMain.getCell(links[i].source().id);
                    break;
                }
            }
        }
        return classAssoc;
    }


    function getInfoLink(link){
        var info;
        if (link.attr('customAttr/type')=='binaryAssociation'){
            info = {
                cardOrigin: link.attributes.labels[0].attrs.text.text,
                cardDestino: link.attributes.labels[1].attrs.text.text,
                roleOrigin: link.attributes.labels[2].attrs.text.text,
                roleDestino: link.attributes.labels[3].attrs.text.text,
                nameAssociation: link.attributes.labels[4].attrs.text.text
            }
        }else if (link.attr('customAttr/type')=='class2rombo'){
            info = {
                cardOrigin: link.attributes.labels[0].attrs.text.text,
                roleOrigin: link.attributes.labels[1].attrs.text.text
            }
        }
        return info;
    }

    function cleanInference() {
	//console.log(palette.model.attributes.cells.models[0]);
  var elements = graphMain.getCells();
  for (var i = 0; i < elements.length; i++) {
      unmarkElement(elements[i]);
  }
}


function markElement(element,tipo) {
	var color;
	switch(tipo) {
        case 'semantico':
        color = 'red';
        break;
        case 'sintactico':
        color = 'yellow';
        break;
        case 'selectGeneralizationType':
        color = 'blue';
        break;
    }
    element.attr('.uml-class-name-rect/stroke',color);
    element.attr('.uml-class-name-rect/stroke-width',2);
	//element.attr('.outer/stroke',color);
}

function unmarkElement(element) {
	var defaultStroke;
	for (var i = 0; i < paletteElements.length; i++) {
		if (paletteElements[i].attributes.type.includes(element.attributes.type)) {
			//obtiene el stroke por defecto del elemento. Se basa en el valor que tiene en la paleta
			defaultStroke = paletteElements[i].attr('.uml-class-name-rect/stroke');
		}
	}
    element.attr('.uml-class-name-rect/stroke',defaultStroke);
    element.attr('.uml-class-name-rect/stroke-width',0.5);
}

//ejemplo para testear
/*var ent1 = paletteElements[0].clone();
var ent2 = paletteElements[0].clone();
var ent3 = paletteElements[0].clone();
var ent4 = paletteElements[0].clone();
var attk1 = paletteElements[3].clone();
var attk2 = paletteElements[3].clone();
var attk3 = paletteElements[3].clone();
var attk4 = paletteElements[3].clone();
graphMain.addCells([ent1,ent2,ent3,ent4,attk1,attk2,attk3,attk4]);
var link1 = createLink();
var link2 = createLink();
var link3 = createLink();
var link4 = createLink();
connectLink(link3, ent1,attk3);
connectLink(link4, ent2,attk4);
connectLink(link1, ent3,attk1);
connectLink(link2, ent4,attk2);
*/