
//ELEMENTOS ORM
joint.dia.Element.define('Orm.Role_1', {
    size: { width: 35, height: 20 },	
    attrs: {			
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'      
         },
        '.oto': {
			'x1':5,
			'y1':-8 ,
			'x2':30,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'			
        },
        '.role_name': {
			text: 'role name',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'			
        },
        root: { magnet: false } 
    },
	 ports: {
        groups: {
            'full': {
                markup: [{ 
					tagName: 'rect', 
					selector: 'portBody', 
					attributes: { 
						width:34, 
						height:20,						
						x:0,
						y:-10,
						rx:3,
						ry:3,
						strokeWidth: 2,
						//stroke: '#333', fill: '#82FA58'
						stroke: 'transparent', fill: 'transparent'

						} 
						}],
                z: -1,
                attrs: { portBody: { magnet: false} },
                position: { name: 'left' },
                label: { position: { name: 'left' } }
            }            
        }
    }
}, {
    markup: '<g class="rotatable scalable"> <rect class="inner" /><line class="oto" /><text class="role_name"/></g>',
});

joint.dia.Element.define('Orm.Role_2', {
    size: { width: 70, height: 20 },
    attrs: {        
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'
        },
        '.line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: '50%'
         },
        '.mtm': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			'display':'none',
			stroke: '#8A0868'
			
        },
        '.mto': {
			'x1':5,
			'y1':-8 ,
			'x2':30,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			'display':'none',
			stroke: '#8A0868'
			
        },
        '.otm': {
			'x1':40,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			'display':'none',
			stroke: '#8A0868'
			
        },
        '.oto': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			'stroke-dasharray':'25 10',
			//'display':'none',
			stroke: '#8A0868'
        },
        '.role_name': {
			text: 'role name',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'
			
        },
        root: { magnet: false } 
     },
	 
	 ports: {
        groups: {
            'left': {
                markup: [{ 
					tagName: 'rect', 
					selector: 'portBody', 
					attributes: { 
						width:30, 
						height:20,
						x:0,
						y:-10,
						rx:3,
						ry:3,
						strokeWidth: 2,
						 
						stroke: '#333', fill: '#82FA58',
						stroke: 'transparent', fill: 'transparent',
						stroke: 'red', fill: 'red'

						} 
						}],
                z: -1,
				
                //attrs: { portBody: { magnet: passive} },
                attrs: { portBody: { magnet: false} },
                position: { name: 'left' },
                label: { position: { name: 'left' } }
            },
            'right': {
                 markup: [{ 
					tagName: 'rect', 
					selector: 'portBody', 
					attributes: { 
						width:30, 
						height:20,
						x:-30,
						y:-10,
						rx:3,
						ry:3,
						strokeWidth: 2,
						stroke: '#333', fill: '#A9BCF5',
						//stroke: 'transparent', fill: 'transparent'

						} 
						}],
                z: -1,
                attrs: { portBody: { magnet: false } },
                position: { name: 'right' },
                label: { position: { name: 'right' } }
            }
        }
    }
}, {
    markup: '<g class="rotatable scalable"> <rect class="inner"/><line class="line_1"/>	<line class="mtm"/>	<line class="mto"/>	<line class="otm"/>	<line class="oto"/>	</g><text class="role_name"/>',
});

joint.dia.Element.define('Orm.Role_3', {
    size: { width: 105, height: 20 },
    attrs: {        
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'
        },
        '.line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 35
          },
        '.line_2': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 70
         },
        '.line_up': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'
			
        },
        '.role_name': {
			text: 'role.....name',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'
			
        },
		root: { magnet: false } // Disable the possibility to connect the body of our shape. Only ports can be connected.
    },
	 
	 ports: {
        groups: {
            'left': {
                markup: [{ 
					tagName: 'rect', 
					selector: 'portBody', 
					attributes: { 
						width:30, 
						height:20,
						x:0,
						y:-10,
						rx:3,
						ry:3,
						
						strokeWidth: 2,
						stroke: '#333', fill: '#82FA58'
						//stroke: 'transparent', fill: 'transparent'

						} 
						}],
                z: -1,
                attrs: { portBody: { magnet: false} },
                position: { name: 'left' },
                label: { position: { name: 'left' } }
            },
            'middle': {
                 markup: [{ 
					tagName: 'rect', 
					selector: 'portBody', 
					attributes: { 
						width:27, 
						height:20,
						x:-66,
						y:-10,
						rx:3,
						ry:3,
						strokeWidth: 2,
						stroke: '#333', fill: '#A9BCF5'
						//stroke: 'transparent', fill: 'transparent'

						} 
						}],
                z: -1,
                attrs: { portBody: { magnet: false } },
                position: { name: 'right' },
                label: { position: { name: 'right' } }
            },
            'right': {
                 markup: [{ 
					tagName: 'rect', 
					selector: 'portBody', 
					attributes: { 
						width:30, 
						height:20,
						x:-30,
						y:-10,
						rx:3,
						ry:3,
						strokeWidth: 2,
						stroke: '#333', fill: '#F78181'
						//stroke: 'transparent', fill: 'transparent'

						} 
						}],
                z: -1,
                attrs: { portBody: { magnet: false } },
                position: { name: 'right' },
                label: { position: { name: 'right' } }
            }
        }
    }
	
	
	
	
}, {
    markup: '<g class="rotatable scalable"> <rect class="inner"/><line class="line_1"/><line class="line_2"/><line class="line_up"/><text class="role_name"/></g>',
});

//----------------------------------ENTITY
joint.dia.Element.define('orm.Entity', {
	size: { width: 150, height: 50 },
    attrs: {
        body: {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2.5,
			rx:10,ry:10,
			stroke: '#0000ad',
            fill: '#FFF'
        },
        label: {
			text: 'Entity',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            refY: '50%',
            fontSize: 16,
            fill: '#333333'
        }
    }
}, {
    markup: [{
        tagName: 'rect',
        selector: 'body',
    }, {
        tagName: 'text',
        selector: 'label'
    }]
});
//----------------------------------FINAL ENTITY

//----------------------------------ENTITY REF MODE
joint.dia.Element.define('orm.EntityRefMode', {
	size: { width: 150, height: 50 },
    attrs: {
        body: {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2.5,
			rx:10,ry:10,
			stroke: '#0000ad',
            fill: '#FFF'
        },
        label: {
			text: 'Entity',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            refY: '30%',
            fontSize: 16,
            fill: '#333333'
        },
        refMode: {
			text: '(ref. mode)',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
			'font-weight':'bold',
            refX: '50%',
            refY: '70%',
            fontSize: 14,
            fill: '#333333'
        }
    }
}, {
    markup: [{
        tagName: 'rect',
        selector: 'body',
    }, {
        tagName: 'text',
        selector: 'label'
    }, {
        tagName: 'text',
        selector: 'refMode'
    }]
});
//----------------------------------FINAL ENTITY  REF MODE

//----------------------------------VALUE
joint.dia.Element.define('orm.Value', {
	size: { width: 150, height: 50 },
    attrs: {
        body: {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2.5,
			'stroke-dasharray':'8 4',
			rx:10,ry:10,
			stroke: '#0000ad',
            fill: '#FFF'
        },
        label: {
			text: 'Value',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            refY: '50%',
            fontSize: 16,
            fill: '#333333'
        }
    }
}, {
    markup: [{
        tagName: 'rect',
        selector: 'body',
    }, {
        tagName: 'text',
        selector: 'label'
    }]
});
//----------------------------------FINAL VALUE

//----------------------------------CONSTRAINTS
joint.dia.Element.define('Orm.Disj', {
    
    attrs: {
        
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'
        },
        '.line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 35
          },
        '.line_2': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 70
         },
        '.line_up': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'
			
        },
        '.role_name': {
			text: 'role.....name',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'			
        }
    }
}, {
	markup: '<g class="rotatable scalable"><ellipse cx="0" cy="0" id="svg_2" stroke="#a000a0" ry="18" rx="18" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="white"/>   <ellipse cx="0" cy="0" id="svg_2" stroke="#a000a0" ry="9" rx="9" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="#a000a0"/></g>'
    
});

joint.dia.Element.define('Orm.Disj2', {
    
    attrs: {
        
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'
        },
        '.line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 35
          },
        '.line_2': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 70
         },
        '.line_up': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'
			
        },
        '.role_name': {
			text: 'role.....name',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'
			
        }
    }
	
	
	
}, {
	markup: '<g class="rotatable scalable"><ellipse cx="0" cy="0" id="svg_2" stroke="#a000a0" ry="18" rx="18" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="white"/>   <line stroke-linecap="null" stroke-linejoin="null" id="svg_3" x2="12.98111" y2="12.92465" y1="-11.71622" x1="-13.85748" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="none" stroke="#a000a0"/>   <line stroke="#a000a0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="-11.74654" y2="12.40336" y1="-12.57387" x1="12.87015" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="none"/>   <ellipse cx="0" cy="0" id="svg_2" stroke="#a000a0" ry="9" rx="9" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="#a000a0"/></g>'
    
});

joint.dia.Element.define('Orm.Disj3', {
    
    attrs: {
        
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'
        },
        '.line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 35
          },
        '.line_2': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 70
         },
        '.line_up': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'
			
        },
        '.role_name': {
			text: 'role.....name',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'
			
        }
    }
	
	
	
}, {
	markup: '<g class="rotatable scalable">    <ellipse fill="white" stroke-width="2" stroke-opacity="null" fill-opacity="null" rx="18" ry="18" stroke="#a000a0" id="svg_2" cy="0" cx="0"/>  <line stroke="#a000a0" fill="none" stroke-width="2" stroke-opacity="null" fill-opacity="null" x1="-13.85748" y1="-11.71622" y2="12.92465" x2="12.98111" id="svg_3" stroke-linejoin="null" stroke-linecap="null"/>  <line fill="none" stroke-width="2" stroke-opacity="null" fill-opacity="null" x1="12.87015" y1="-12.57387" y2="12.40336" x2="-11.74654" id="svg_8" stroke-linejoin="null" stroke-linecap="null" stroke="#a000a0"/></g>'
    
});

joint.dia.Element.define('Orm.Disj4', {
    
    attrs: {
        
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'
        },
        '.line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 35
          },
        '.line_2': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 70
         },
        '.line_up': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'
			
        },
        '.role_name': {
			text: 'role.....name',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'
			
        }
    }
	
	
	
}, {
	markup: '<g class="rotatable scalable">   <ellipse id="svg_2" stroke="#a000a0" ry="18" rx="18" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="white"/>  <line stroke="#a000a0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="10" y2="4" y1="4" x1="-10" fill-opacity="null" stroke-opacity="null" stroke-width="2.5" fill="none"/>  <line stroke="#a000a0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="10" y2="-4" y1="-4" x1="-10" fill-opacity="null" stroke-opacity="null" stroke-width="2.5" fill="none"/> </g>'    
});

joint.dia.Element.define('Orm.Disj5', {
    
    attrs: {
        
        '.inner': {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#333',
			//refX: '100%',
			'rx':3,
			'ry':3,
            fill: '#fff'
        },
        '.line_1': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 35
          },
        '.line_2': {
			'x1':0,
			'y1':0 ,
			'x2':0,
			'y2':20,
			strokeWidth: 2,
			stroke: '#333',
			refX: 70
         },
        '.line_up': {
			'x1':5,
			'y1':-8 ,
			'x2':65,
			'y2':-8,
			'stroke-linecap':'round',
			strokeWidth: 3,
			stroke: '#8A0868'
			
        },
        '.role_name': {
			text: 'role.....name',
            'font-family': 'Arial', 
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            y: 35,
            fontSize: 14,
            fill: '#333333'
			
        }
    }
	
	
	
}, {
	markup: '<g class="rotatable scalable">   <ellipse id="svg_2" stroke="#a000a0" ry="18" rx="18" fill-opacity="null" stroke-opacity="null" stroke-width="2" fill="white"/> <line stroke="#a000a0" stroke-linecap="null" stroke-linejoin="null" id="svg_8" x2="9" y2="9" y1="9" x1="-9" fill-opacity="null" stroke-opacity="null" stroke-width="2.5" fill="none"/>  <text fill="#a000a0" stroke="#000000" stroke-width="0" x="-11.99954" y="6.9994" id="svg_11" font-size="28" font-family="Helvetica, Arial, sans-serif" text-anchor="start" xml:space="preserve" transform="rotate(90 -1.8749999999999998,-2.5000000000000004) ">U</text></g>'    
});

//----------------------------------FIN CONSTRAINTS
joint.shapes.orm.ISA = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.ISA',
	attrs: { 
	'.connection': { stroke: '#a000a0', 'stroke-width': 4 },
	//'.marker-source': { stroke: '#fe854f', fill: '#fe854f', d: 'M 10 0 L 0 5 L 10 10 z' },
	'.marker-target': { stroke: '#a000a0', fill: '#a000a0', d: 'M 13 0 L 0 7 L 13 14 z' }
	}
        
    }, joint.dia.Link.prototype.defaults),


});

joint.shapes.orm.ISA_Disj = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.ISA_Disj',
	attrs: { 
	'.connection': { stroke: '#a000a0', 'stroke-width': 4 }
	//'.marker-source': { stroke: '#fe854f', fill: '#fe854f', d: 'M 10 0 L 0 5 L 10 10 z' },
	//'.marker-target': { stroke: '#a000a0', fill: '#a000a0', d: 'M 13 0 L 0 7 L 13 14 z' }
	}
        
    }, joint.dia.Link.prototype.defaults),


});

joint.shapes.orm.ISA_Double = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.ISA',
	attrs: { 
	'.connection': { stroke: '#a000a0', 'stroke-width': 4 },
	//'.marker-source': { stroke: '#fe854f', fill: '#fe854f', d: 'M 10 0 L 0 5 L 10 10 z' },
	'.marker-target': { stroke: '#a000a0', fill: '#a000a0', d: 'M 13 0 L 0 7 L 13 14 z' }
	}
        
    }, joint.dia.Link.prototype.defaults),


});

joint.shapes.orm.Dashed_Link = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.Dashed_Link',
	attrs: { 
	'.connection': { stroke: 'blue', 'stroke-width': 2,'stroke-dasharray': '7 3', 'fill': 'blue',    },
	
	'.marker-source': { stroke: '#a000a0', fill: '#a000a0', d: 'M 10 0 L 0 5 L 10 10 z','z-index': -1  },
	'.marker-target': { stroke: '#a000a0', fill: '#a000a0', d: 'M 13 0 L 0 7 L 13 14 z','z-index': -1  }
	
	}
        
    }, joint.dia.Link.prototype.defaults),

	

});

joint.shapes.orm.Link = joint.dia.Link.extend({

    defaults: _.defaultsDeep({

        type: 'orm.Link',
	attrs: { 
	'.connection': { stroke: '#a000a0', 'stroke-width': 2 },
	//'.marker-source': { stroke: '#fe854f', fill: '#fe854f', d: 'M 10 0 L 0 5 L 10 10 z' },
	//'.marker-target': { stroke: '#9100DA', fill: '#9100DA', d: 'M 13 0 L 0 7 L 13 14 z' }
	}
        
    }, joint.dia.Link.prototype.defaults),


});

joint.shapes.orm.Mandatory = joint.dia.Link.extend({

    defaults: _.defaultsDeep({
	
        type: 'orm.Mandatory',
        attrs: { 
	'.connection': { stroke: '#a000a0', 'stroke-width': 2 },
	'.marker-source': { stroke: '#a000a0', fill: '#a000a0', d: 'M 0 0 a 8 8 0 1 0 0 1' }
	}
        
    }, joint.dia.Link.prototype.defaults),


});
    
//FUNCIONES UTILIZADAS EN HTML PARA INTERACTUAR

function change_state(valor_estado)	{
		switch(valor_estado) {
				case 'UR':
					$('#lbl_estado_actual').html('<strong>ADD UNARY ROLE</strong>');
					$('#lbl_elementos_necesarios').html('<strong>1</strong>');
					elementos_seleccionados=[];
					cantidad_elementos_seleccionar=1;
					estado_actual=1;
					break;
				case 'BR':
					$('#lbl_estado_actual').html('<strong>ADD BINARY ROLE</strong>');
					$('#lbl_elementos_necesarios').html('<strong>2</strong>');
					elementos_seleccionados=[];
					cantidad_elementos_seleccionar=2;
					estado_actual=2;
					break;
				case 'TR':
					$('#lbl_estado_actual').html('<strong>ADD TERNARY ROLE</strong>');
					$('#lbl_elementos_necesarios').html('<strong>3</strong>');
					elementos_seleccionados=[];
					cantidad_elementos_seleccionar=3;
					estado_actual=5;
					break;
				case 'DIS':
					$('#lbl_estado_actual').html('<strong>ADD DISJOINT</strong>');
					$('#lbl_elementos_necesarios').html('<strong>2</strong>');
					elementos_seleccionados=[];
					cantidad_elementos_seleccionar=2;
					estado_actual=3;
					break;
				case 'ISA':
					$('#lbl_estado_actual').html('<strong>ADD ISA</strong>');
					$('#lbl_elementos_necesarios').html('<strong>2</strong>');
					elementos_seleccionados=[];
					cantidad_elementos_seleccionar=2;
					estado_actual=4;
					break;
				case 'ISA_D':
					$('#lbl_estado_actual').html('<strong>ADD ISA DISJ</strong>');
					$('#lbl_elementos_necesarios').html('<strong>3</strong>');
					elementos_seleccionados=[];
					cantidad_elementos_seleccionar=3;
					estado_actual=6;
					break;
				case 'REMOVE':
					$('#lbl_estado_actual').html('<strong>REMOVE ELEMENT</strong>');
					$('#lbl_elementos_necesarios').html('<strong>1</strong>');
					elementos_seleccionados=[];
					cantidad_elementos_seleccionar=1;
					estado_actual=9;
					break;					
			}
	}

function change_disj(css_value)	{	
	var elto=graph.getCell($('#cid').text());
	var elto_pos=elto.position();
	//alert(elto);
	var view_a=paper.findViewByModel(elto);
	
	var tipos=["Orm.Disj","Orm.Disj2","Orm.Disj3","Orm.Disj4","Orm.Disj5"];
	
	if ($.inArray(view_a.model.attributes.type, tipos)>=0) 
	{
		//alert(view_a);
		var outboundLinks = graph.getConnectedLinks(elto);
		//alert(outboundLinks);
		switch($('#ddl_disj').val()) {
			case 'D1':
				var nuevo_elto= new Orm.Disj({});
			break;
			
			case 'D2':
				var nuevo_elto= new Orm.Disj2({});
			break;
			
			case 'D3':
				var nuevo_elto= new Orm.Disj3({});
			break;
			
			case 'D4':
				var nuevo_elto= new Orm.Disj4({});
			break;
			
			case 'D5':
				var nuevo_elto= new Orm.Disj5({});
			break;
		}
					
		graph.addCell(nuevo_elto);
		nuevo_elto.position(elto_pos.x,elto_pos.y);
		
		jQuery.each( outboundLinks, function( i, val ) {
			if((val.source().id) ==graph.getCell($('#cid').text()).id)
			{
				val.source(nuevo_elto);
			}
			
			if((val.target().id) ==graph.getCell($('#cid').text()).id)
			{
				val.target(nuevo_elto);
			}
		});
		graph.removeCells(elto);			
		$('#cid').text((paper.findViewByModel(nuevo_elto)).model.cid);
	}
}
	
function change_line(css_value)	{
	var elto=graph.getCell($('#cid').text());
	//alert(elto);
	var view_a=paper.findViewByModel(elto);
	if (view_a.model.attributes.type=='Orm.Role_2') 
		{
		
		view_a.$('.oto').css('display','none');
		view_a.$('.mtm').css('display','none');
		view_a.$('.mto').css('display','none');
		view_a.$('.otm').css('display','none');
		
		switch(css_value) {
			case 'mtm':
				view_a.$('.mtm').css('display','inline')
				break;
			case 'oto':
				view_a.$('.oto').css('display','inline')
				break;
			case 'mto':
				view_a.$('.mto').css('display','inline')
				break;
			case 'otm':
				view_a.$('.otm').css('display','inline')
				break;				
		}
	}
	else if (view_a.model.attributes.type=='Orm.Role_1') 
		{
			view_a.$('.oto').css('display','none');
			switch(css_value) {
				case 'oto':
					view_a.$('.oto').css('display','inline')
					break;				
			}
		}
	else
	{alert('No support function for this type of ELEMENT: ' +view_a.model.attributes.type );}		
}
	
function create_role(array_eltos){

	switch(estado_actual) {
	//UNARY ROLE
	case 1:
		var nombre_Role = prompt("Nombre del ROLE", "");
		var role = new Orm.Role_1({
		attrs:{'.role_name': {text: nombre_Role}},
		ports: { items: [{ id:'left_id',group: 'full' }]}});
		var new_link = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[0]},
			target: {
				id: role.id,
				port: 'left_id'
			}
		});
		
		graph.addCell(role);
		graph.addCell(new_link);		
		
		var elto=graph.getCell(array_eltos[0]);
		var pos=elto.position();
		var width=elto.attributes.size.width;
		role.position((pos.x+width+50),pos.y);
		
		cancel();
		break;
	
	//BINARY ROLE
	case 2:
		
		var nombre_Role = prompt("Nombre del ROLE", "");
		var role = new Orm.Role_2({
		attrs:{'.role_name': {text: nombre_Role}},
		ports: { items: [{ id:'left_id',group: 'left' },{id:'right_id', group: 'right'}]}});
		
		var new_link_L = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[0]},
			target: {
				id: role.id,
				port: 'left_id'
			}
		});
		
		var new_link_R = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[1]},
			target: {
				id: role.id,
				port: 'right_id'
			}
		});
		
		graph.addCell(role);
		graph.addCell(new_link_R);
		graph.addCell(new_link_L);
		
		var nueva_posicion=calcular_posicion(array_eltos);
		role.position(nueva_posicion[0],nueva_posicion[1]);
		
		
		cancel();
		break;
	case 3:
		
		switch($('#ddl_disj').val()) {
			case 'D1':
				var d= new Orm.Disj({});
			break;
			
			case 'D2':
				var d= new Orm.Disj2({});
			break;
			
			case 'D3':
				var d= new Orm.Disj3({});
			break;
			
			case 'D4':
				var d= new Orm.Disj4({});
			break;
			
			case 'D5':
				var d= new Orm.Disj5({});
			break;
		}
		
		d.addTo(graph);
		var new_link_L = new joint.shapes.devs.Link();
		new_link_L.source(graph.getCell(array_eltos[0]));
		new_link_L.target(d);
		
		var new_link_R = new joint.shapes.devs.Link();
		new_link_R.source(graph.getCell(array_eltos[1]));
		new_link_R.target(d);
		
		new_link_R.addTo(graph);
		new_link_L.addTo(graph);
		
		var nueva_posicion=calcular_posicion(array_eltos);
		d.position(nueva_posicion[0],nueva_posicion[1]);
		cancel();
		break
		
	case 4:
		var new_link_R = new joint.shapes.orm.ISA();			
		new_link_R.source(graph.getCell(array_eltos[1]));
		new_link_R.target(graph.getCell(array_eltos[0]));
		
		
		new_link_R.addTo(graph);
		
		cancel();
		break;
	//TERNARY ROLE
	case 5:
		
		var role = new Orm.Role_3({
		ports: { items: [{ id:'left_id',group: 'left' },{id:'middle_id', group: 'middle'},{id:'right_id', group: 'right'}]}});
		
		
		var new_link_L = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[0]},
			target: {
				id: role.id,
				port: 'left_id'
			}
		});
		
		var new_link_M = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[1]},
			target: {
				id: role.id,
				port: 'middle_id'
			}
		});
		
		var new_link_R = new joint.shapes.devs.Link({
			source: {
			id: array_eltos[2]},
			target: {
				id: role.id,
				port: 'right_id'
			}
		});
		
		graph.addCell(role);
		graph.addCell(new_link_R);
		graph.addCell(new_link_M);
		graph.addCell(new_link_L);
		
		var nueva_posicion=calcular_posicion(array_eltos);
		role.position(nueva_posicion[0],nueva_posicion[1]);
		
		
		cancel();
		break;
	//ISA DISJOINT
	case 6:
		
		switch($('#ddl_disj').val()) {
		
		
		case 'D1':
			var d= new Orm.Disj({});
		break;
		
		case 'D2':
			var d= new Orm.Disj2({});
		break;
		
		case 'D3':
			var d= new Orm.Disj3({});
		break;
		
		case 'D4':
			var d= new Orm.Disj4({});
		break;
		
		case 'D5':
			var d= new Orm.Disj5({});
		break;
		}
		
		d.addTo(graph);
						 
		var new_link_L = new joint.shapes.orm.ISA_Disj();
		new_link_L.source(graph.getCell(array_eltos[1]));
		new_link_L.target(d);
		
		var new_link_R = new joint.shapes.orm.ISA_Disj();
		new_link_R.source(graph.getCell(array_eltos[2]));
		new_link_R.target(d);
		
		var new_link_P = new joint.shapes.orm.ISA();
		new_link_P.target(graph.getCell(array_eltos[0]));
		new_link_P.source(d);
		
		new_link_R.addTo(graph);
		new_link_L.addTo(graph);
		new_link_P.addTo(graph);
		
		var nueva_posicion=calcular_posicion(array_eltos);
		d.position(nueva_posicion[0],nueva_posicion[1]);
		cancel();
		break
	}
	
}

function calcular_posicion(array_eltos){
			
		var posiciones =[];
		var elto_a=graph.getCell(array_eltos[0]);
		var elto_b=graph.getCell(array_eltos[1]);
		
		var pos_a=elto_a.position();
		var pos_b=elto_b.position();
		
		var y_pos_new=pos_a.y;
		var x_pos_new=pos_a.x;
		
		var width_a=elto_a.attributes.size.width;
		var width_b=elto_b.attributes.size.width;
		
		var height_a=elto_a.attributes.size.height;
		var height_b=elto_b.attributes.size.height;
		
		if(pos_a.y>pos_b.y)
		{
			y_pos_new=((pos_a.y-pos_b.y)/2)+pos_b.y+(height_b/2);
			//alert(pos_a.y + '-' +pos_b.y + '-' +y_pos_new);
		}
		else
		{
			y_pos_new=((pos_b.y-pos_a.y)/2)+pos_a.y+(height_a/2);
			//alert(pos_a.y + '-' +pos_b.y + '-' +y_pos_new);
		}
		
		
		if(pos_a.x>pos_b.x)
		{
			x_pos_new=((pos_a.x-pos_b.x)/2)+pos_b.x+(width_b/2);
			
		}
		else
		{
			
			x_pos_new=((pos_b.x-pos_a.x)/2)+pos_a.x+(width_a/2);
		}
		
		posiciones.push(x_pos_new,y_pos_new);
		return posiciones;
}